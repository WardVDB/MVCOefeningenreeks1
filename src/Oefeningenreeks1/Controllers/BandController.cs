﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHobbies;

namespace Oefeningenreeks1.Controllers
{
    public class BandController : Controller
    {
        private List<Band> bands;
        public BandController()
        {
            bands = new List<Band>() {
                new Band() { Naam="The Beatles", Jaar=1960,
                    Leden = new List<BandLid>()
                        {
                            new BandLid() { Naam="George", Geslacht=Geslacht.Man, Leeftijd=55, Levend=false },
                            new BandLid() { Naam="Ringo", Geslacht=Geslacht.Man, Leeftijd=64, Levend=true },
                            new BandLid() { Naam="Paul", Geslacht=Geslacht.Man, Leeftijd=76, Levend=true },
                            new BandLid() { Naam="John", Geslacht=Geslacht.Man, Leeftijd=35, Levend=false }
                        }                           
                },
                new Band() { Naam="Nicole & Hugo", Jaar=1302,
                    Leden = new List<BandLid>()
                        {
                            new BandLid() { Naam="Nicole", Geslacht=Geslacht.Vrouw, Leeftijd=105, Levend=true },
                            new BandLid() { Naam="Hugo", Geslacht=Geslacht.Man, Leeftijd=103, Levend=true },
                        }
                }
            };
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Lijst()
        {
            ViewBag.Bands = bands;
            return View();
        }

        public IActionResult LijstMetLeden()
        {
            ViewBag.Bands = bands;
            return View();
        }

        public IActionResult Maak(string naam, int jaar)
        {
            bands.Add(new Band() { Naam=naam, Jaar=jaar });
            ViewBag.Bands = bands;
            return View("Lijst");
        }

        public IActionResult JSLijst()
        {
            return Json(bands);
        }
    }
}
